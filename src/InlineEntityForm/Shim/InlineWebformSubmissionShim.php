<?php

declare(strict_types=1);
namespace Drupal\webform4json\InlineEntityForm\Shim;

use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Entity\ContentEntityFormInterface;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityFormInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element;
use Drupal\Core\Render\ElementInfoManagerInterface;
use Drupal\h4d_shared\DumperTool;

/**
 * Inline WebformSubmission Shim.
 *
 * Must fool webform build, process, validate.
 * But not FormBuilder.
 * So...
 * - Fool form object with fake form.
 *   @see \Drupal\Core\Entity\EntityForm::afterBuild
 *   @see \Drupal\Core\Entity\ContentEntityForm::validateForm
 *   @see \Drupal\Core\Entity\EntityForm::submitForm
 * - Fool all callbacks with fake FormState.
 */
final class InlineWebformSubmissionShim {

  protected const storageProperty = '#inline_webform_submission_shim';

  public function __construct(
    protected array $arrayParents,
    protected EntityFormInterface $formObject,
  ) {}

  public static function create(
    array $form,
    ContentEntityInterface $entity,
    string $formOperation = 'edit',
  ): self {
    $entityTypeId = $entity->getEntityTypeId();
    $arrayParents = $form['#array_parents'] ?? throw new \UnexpectedValueException('Need form #array_parents.');
    $formObject = \Drupal::entityTypeManager()
      ->getFormObject($entityTypeId, $formOperation);
    // Because $entity instanceof ContentEntityInterface.
    assert($formObject instanceof ContentEntityFormInterface);
    $formObject->setEntity($entity);
    // $wrappedFormObject = new FixBuildContentEntityFormWrapper($formObject, $form);
    return new self($arrayParents, $formObject);
  }

  public function getArrayParents(): array {
    return $this->arrayParents;
  }

  public function getFormObject(): EntityFormInterface {
    return $this->formObject;
  }

}
