<?php

declare(strict_types=1);
namespace Drupal\webform4json\InlineEntityForm\Shim;

use Drupal\Core\Entity\EntityFormInterface;
use Drupal\Core\Form\FormStateDecoratorBase;
use Drupal\Core\Form\FormStateInterface;

final class InlineFormStateShim extends FormStateDecoratorBase {

  public function __construct(
    FormStateInterface $decoratedFormState,
    protected array &$completeForm,
    protected EntityFormInterface $formObject,
  ) {
    $this->decoratedFormState = $decoratedFormState;
  }

  public function &getCompleteForm() {
    return $this->completeForm;
  }

  public function getFormObject() {
    return $this->formObject;
  }

  public function getParentFormState(): FormStateInterface {
    return $this->decoratedFormState;
  }

  public static function getRootFormState(FormStateInterface $formState): FormStateInterface {
    return $formState instanceof InlineFormStateShim
      ? self::getRootFormState($formState->getParentFormState())
      : $formState;
  }

  public function prepareCallback($callback) {
    if (is_string($callback) && str_starts_with($callback, '::')) {
      $callback = [$this->getFormObject(), substr($callback, 2)];
    }
    return $callback;
  }

}
