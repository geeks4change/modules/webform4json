<?php

declare(strict_types=1);
namespace Drupal\webform4json\InlineEntityForm\Shim\RecursiveCallback;

use Drupal\Component\Render\MarkupInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element;
use Drupal\Core\Security\Attribute\TrustedCallback;
use Drupal\webform4json\InlineEntityForm\Shim\RecursiveCallback\Interface\AfterBuildInterface;
use Drupal\webform4json\InlineEntityForm\Shim\RecursiveCallback\Interface\AfterBuildWrapperInterface;
use Drupal\webform4json\InlineEntityForm\Shim\RecursiveCallback\Interface\ElementValidateInterface;
use Drupal\webform4json\InlineEntityForm\Shim\RecursiveCallback\Interface\ElementValidateWrapperInterface;
use Drupal\webform4json\InlineEntityForm\Shim\RecursiveCallback\Interface\PostRenderInterface;
use Drupal\webform4json\InlineEntityForm\Shim\RecursiveCallback\Interface\PostRenderWrapperInterface;
use Drupal\webform4json\InlineEntityForm\Shim\RecursiveCallback\Interface\PrepareInterface;
use Drupal\webform4json\InlineEntityForm\Shim\RecursiveCallback\Interface\PreRenderInterface;
use Drupal\webform4json\InlineEntityForm\Shim\RecursiveCallback\Interface\PreRenderWrapperInterface;
use Drupal\webform4json\InlineEntityForm\Shim\RecursiveCallback\Interface\ProcessInterface;
use Drupal\webform4json\InlineEntityForm\Shim\RecursiveCallback\Interface\ProcessWrapperInterface;
use Drupal\webform4json\InlineEntityForm\Shim\RecursiveCallback\TypedCallback\AfterBuildCallback;
use Drupal\webform4json\InlineEntityForm\Shim\RecursiveCallback\TypedCallback\ElementValidateCallback;
use Drupal\webform4json\InlineEntityForm\Shim\RecursiveCallback\TypedCallback\PostRenderCallback;
use Drupal\webform4json\InlineEntityForm\Shim\RecursiveCallback\TypedCallback\PreRenderCallback;
use Drupal\webform4json\InlineEntityForm\Shim\RecursiveCallback\TypedCallback\ProcessCallback;
use Drupal\webform4json\InlineEntityForm\Utility\ElementDefaults;
use Drupal\webform4json\InlineEntityForm\Utility\Objects;

final class RecursiveCallbackWrapper {

  protected const StorageProperty = '#' . self::class;

  private function __construct(
    private readonly array $helpers,
  ) {}

  public static function create(
    PrepareInterface
    |ProcessInterface|AfterBuildInterface|ElementValidateInterface
    |PreRenderInterface|PostRenderInterface
    |ProcessWrapperInterface|AfterBuildWrapperInterface|ElementValidateWrapperInterface
    |PreRenderWrapperInterface|PostRenderWrapperInterface
    ...$helpers
  ): self {
    return new self($helpers);
  }
  public function prepareDescendants(array &$element): void {
    foreach (Element::children($element) as $key) {
      $this->prepareElement($element[$key]);
    }
  }

  private function prepareElement(array &$element): void {
    foreach (Objects::create(...$this->helpers)->instanceOf(PrepareInterface::class) as $prepare) {
      $prepare->prepare($element);
    }

    // Store away the next callbacks.
    ElementDefaults::load($element);
    $element[self::StorageProperty]['#process'] = $element['#process'] ?? [];
    $element['#process'] = [];
    $element['#process'][] = [$this, 'process'];
  }

  public function process(array &$element, FormStateInterface &$formState, array &$completeForm): array {
    // Wrap existing callbacks.
    $processCallbacks = $element[self::StorageProperty]['#process'];
    foreach ($processCallbacks as $processCallback) {
      $process = new ProcessCallback($processCallback);
      foreach (Objects::create(...$this->helpers)->instanceOf(ProcessWrapperInterface::class) as $processWrapper) {
        $process = $processWrapper->wrapProcess($process);
      }
      /** @see \Drupal\Core\Form\FormBuilder::processForm */
      $completeForm = &$formState->getCompleteForm();
      $element = $process->process($element, $formState, $completeForm);
    }

    // Apply own callbacks.
    $ownProcessCallbacks = Objects::create(...$this->helpers)
      ->instanceOf(ProcessInterface::class);
    foreach ($ownProcessCallbacks as $process) {
      /** @see \Drupal\Core\Form\FormBuilder::processForm */
      $completeForm = &$formState->getCompleteForm();
      $element = $process->process($element, $formState, $completeForm);
    }

    // Store away the next callbacks.
    ElementDefaults::load($element);
    $element[self::StorageProperty]['#after_build'] = $element['#after_build'] ?? [];
    $element['#after_build'] = [];
    $element['#after_build'][] = [$this, 'afterBuild'];

    $this->prepareDescendants($element);

    return $element;
  }

  public function afterBuild(array $element, FormStateInterface &$formState): array {
    // Wrap existing callbacks.
    $afterBuildCallbacks = $element[self::StorageProperty]['#after_build'];
    foreach ($afterBuildCallbacks as $afterBuildCallback) {
      $afterBuild = new AfterBuildCallback($afterBuildCallback);
      foreach (Objects::create(...$this->helpers)->instanceOf(AfterBuildWrapperInterface::class) as $afterBuildWrapper) {
        $afterBuild = $afterBuildWrapper->wrapAfterBuild($afterBuild);
      }
      $element = $afterBuild->afterBuild($element, $formState);
    }

    // Apply own callbacks.
    $ownAfterBuildCallbacks = Objects::create(...$this->helpers)
      ->instanceOf(AfterBuildInterface::class);
    foreach ($ownAfterBuildCallbacks as $afterBuild) {
      $element = $afterBuild->afterBuild($element, $formState);
    }

    // Store away the next callbacks.
    ElementDefaults::load($element);
    $element[self::StorageProperty]['#element_validate'] = $element['#element_validate'] ?? [];
    $element['#element_validate'] = [];
    $element['#element_validate'][] = [$this, 'elementValidate'];

    $element[self::StorageProperty]['#pre_render'] = $element['#pre_render'] ?? [];
    $element['#pre_render'] = [];
    $element['#pre_render'][] = [$this, 'preRender'];

    return $element;
  }

  public function elementValidate(array &$element, FormStateInterface &$formState, array &$completeForm): void {
    // Wrap existing callbacks.
    $elementValidateCallbacks = $element[self::StorageProperty]['#element_validate'];
    foreach ($elementValidateCallbacks as $elementValidateCallback) {
      $elementValidate = new ElementValidateCallback($elementValidateCallback);
      foreach (Objects::create(...$this->helpers)->instanceOf(ElementValidateWrapperInterface::class) as $elementValidateWrapper) {
        $elementValidate = $elementValidateWrapper->wrapElementValidate($elementValidate);
      }
      $elementValidate->elementValidate($element, $formState, $completeForm);
    }

    // Apply own callbacks.
    $ownElementValidateCallbacks = Objects::create(...$this->helpers)
      ->instanceOf(ElementValidateInterface::class);
    foreach ($ownElementValidateCallbacks as $elementValidate) {
      $elementValidate->elementValidate($element, $formState, $completeForm);
    }

    // Store away the next callbacks not needed.
  }

  #[TrustedCallback]
  public function preRender(array $element): array {
    // Wrap existing callbacks.
    $preRenderCallbacks = $element[self::StorageProperty]['#pre_render'];
    foreach ($preRenderCallbacks as $preRenderCallback) {
      $preRender = new PreRenderCallback($preRenderCallback);
      foreach (Objects::create(...$this->helpers)->instanceOf(PreRenderWrapperInterface::class) as $preRenderWrapper) {
        $preRender = $preRenderWrapper->wrapPreRender($preRender);
      }
      $element = $preRender->preRender($element);
    }

    // Apply own callbacks.
    $ownPreRenderCallbacks = Objects::create(...$this->helpers)
      ->instanceOf(PreRenderInterface::class);
    foreach ($ownPreRenderCallbacks as $preRender) {
      $element = $preRender->preRender($element);
    }

    // Store away the next callbacks.
    ElementDefaults::load($element);
    $element[self::StorageProperty]['#post_render'] = $element['#post_render'] ?? [];
    $element['#post_render'] = [];
    $element['#post_render'][] = [$this, 'postRender'];

    return $element;
  }

  #[TrustedCallback]
  public function postRender(MarkupInterface|\Stringable|string $children, array $element): string|\Stringable|MarkupInterface {
    // Wrap existing callbacks.
    $pstRenderCallbacks = $element[self::StorageProperty]['#post_render'];
    foreach ($pstRenderCallbacks as $postRenderCallback) {
      $postRender = new PostRenderCallback($postRenderCallback);
      foreach (Objects::create(...$this->helpers)->instanceOf(PostRenderWrapperInterface::class) as $postRenderWrapper) {
        $postRender = $postRenderWrapper->wrapPostRender($postRender);
      }
      $children = $postRender->postRender($children, $element);
    }

    // Apply own callbacks.
    $ownPostRenderCallbacks = Objects::create(...$this->helpers)
      ->instanceOf(PostRenderInterface::class);
    foreach ($ownPostRenderCallbacks as $postRender) {
      $children = $postRender->postRender($children, $element);
    }

    // Store away the next callbacks not needed.

    return $children;
  }

}
