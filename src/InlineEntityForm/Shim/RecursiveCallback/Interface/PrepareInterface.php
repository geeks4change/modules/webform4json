<?php

declare(strict_types=1);
namespace Drupal\webform4json\InlineEntityForm\Shim\RecursiveCallback\Interface;

interface PrepareInterface {

  public function prepare(array &$element): void;

}
