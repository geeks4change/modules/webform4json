<?php

declare(strict_types=1);
namespace Drupal\webform4json\InlineEntityForm\Shim\RecursiveCallback\Interface;

use Drupal\Core\Form\FormStateInterface;

interface ElementValidateInterface {

  /**
   * @see \Drupal\Core\Form\FormValidator::doValidateForm
   */
  public function elementValidate(array &$element, FormStateInterface &$formState, array &$completeForm): void;

}
