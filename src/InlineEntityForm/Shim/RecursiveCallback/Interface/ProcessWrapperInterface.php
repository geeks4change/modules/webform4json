<?php

declare(strict_types=1);
namespace Drupal\webform4json\InlineEntityForm\Shim\RecursiveCallback\Interface;

use Drupal\webform4json\InlineEntityForm\Shim\RecursiveCallback\Interface\ProcessInterface;

interface ProcessWrapperInterface {

  public function wrapProcess(ProcessInterface $process): ProcessInterface;

}
