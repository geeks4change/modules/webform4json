<?php

declare(strict_types=1);
namespace Drupal\webform4json\InlineEntityForm\Shim\RecursiveCallback\Interface;

use Drupal\Core\Form\FormStateInterface;

interface AfterBuildInterface {

  /**
   * @see \Drupal\Core\Form\FormBuilder::doBuildForm
   */
  public function afterBuild(array $element, FormStateInterface &$formState): array;

}
