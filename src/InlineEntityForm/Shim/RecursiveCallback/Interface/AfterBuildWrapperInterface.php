<?php

declare(strict_types=1);
namespace Drupal\webform4json\InlineEntityForm\Shim\RecursiveCallback\Interface;

use Drupal\webform4json\InlineEntityForm\Shim\RecursiveCallback\Interface\AfterBuildInterface;

interface AfterBuildWrapperInterface {

  public function wrapAfterBuild(AfterBuildInterface $afterBuild): AfterBuildInterface;

}
