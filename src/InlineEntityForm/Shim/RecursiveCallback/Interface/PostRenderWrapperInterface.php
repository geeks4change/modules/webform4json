<?php

declare(strict_types=1);
namespace Drupal\webform4json\InlineEntityForm\Shim\RecursiveCallback\Interface;

use Drupal\webform4json\InlineEntityForm\Shim\RecursiveCallback\Interface\PostRenderInterface;

interface PostRenderWrapperInterface {

  public function wrapPostRender(PostRenderInterface $postRender): PostRenderInterface;
}
