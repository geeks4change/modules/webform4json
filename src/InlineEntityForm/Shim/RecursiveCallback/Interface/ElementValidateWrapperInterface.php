<?php

declare(strict_types=1);
namespace Drupal\webform4json\InlineEntityForm\Shim\RecursiveCallback\Interface;

use Drupal\webform4json\InlineEntityForm\Shim\RecursiveCallback\Interface\ElementValidateInterface;

interface ElementValidateWrapperInterface {

  public function wrapElementValidate(ElementValidateInterface $elementValidate): ElementValidateInterface;

}
