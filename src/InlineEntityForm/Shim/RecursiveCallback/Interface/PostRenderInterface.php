<?php

declare(strict_types=1);
namespace Drupal\webform4json\InlineEntityForm\Shim\RecursiveCallback\Interface;

use Drupal\Component\Render\MarkupInterface;

interface PostRenderInterface {

  /**
   * @see \Drupal\Core\Render\Renderer::doRender
   */
  public function postRender(string|\Stringable|MarkupInterface $children, array $element): string|\Stringable|MarkupInterface;

}
