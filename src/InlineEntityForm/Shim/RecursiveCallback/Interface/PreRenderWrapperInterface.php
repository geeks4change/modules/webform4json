<?php

declare(strict_types=1);
namespace Drupal\webform4json\InlineEntityForm\Shim\RecursiveCallback\Interface;

use Drupal\webform4json\InlineEntityForm\Shim\RecursiveCallback\Interface\PreRenderInterface;

interface PreRenderWrapperInterface {

  public function wrapPreRender(PreRenderInterface $preRender): PreRenderInterface;

}
