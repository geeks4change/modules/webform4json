<?php

declare(strict_types=1);
namespace Drupal\webform4json\InlineEntityForm\Shim\RecursiveCallback\Interface;

interface PreRenderInterface {

  /**
   * @see \Drupal\Core\Render\Renderer::doRender
   */
  public function preRender(array $element): array;

}
