<?php

declare(strict_types=1);
namespace Drupal\webform4json\InlineEntityForm\Shim\RecursiveCallback\TypedCallback;

use Drupal\Component\Render\MarkupInterface;
use Drupal\webform4json\InlineEntityForm\Shim\RecursiveCallback\Interface\PostRenderInterface;

final class PostRenderCallback implements PostRenderInterface {

  public function __construct(
    private readonly mixed $callback,
  ) {}


  public function postRender(MarkupInterface|\Stringable|string $children, array $element): string|\Stringable|MarkupInterface {
    return ($this->callback)($children, $element);
  }

}
