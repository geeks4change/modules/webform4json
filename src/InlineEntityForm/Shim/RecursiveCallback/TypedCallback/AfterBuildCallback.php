<?php

declare(strict_types=1);
namespace Drupal\webform4json\InlineEntityForm\Shim\RecursiveCallback\TypedCallback;

use Drupal\Core\Form\FormStateInterface;
use Drupal\webform4json\InlineEntityForm\Shim\RecursiveCallback\Interface\AfterBuildInterface;

final class AfterBuildCallback implements AfterBuildInterface {

  public function __construct(
    private readonly mixed $rawCallback,
  ) {}


  public function afterBuild(array $element, FormStateInterface &$formState): array {
    return $formState->prepareCallback($this->rawCallback)($element, $formState);
  }

}
