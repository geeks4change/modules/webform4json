<?php

declare(strict_types=1);
namespace Drupal\webform4json\InlineEntityForm\Shim\RecursiveCallback\TypedCallback;

use Drupal\webform4json\InlineEntityForm\Shim\RecursiveCallback\Interface\PreRenderInterface;

final class PreRenderCallback implements PreRenderInterface {

  public function __construct(
    private readonly mixed $callback,
  ) {}

  public function preRender(array $element): array {
    return ($this->callback)($element);
  }

}
