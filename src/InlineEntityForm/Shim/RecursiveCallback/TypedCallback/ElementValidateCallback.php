<?php

declare(strict_types=1);
namespace Drupal\webform4json\InlineEntityForm\Shim\RecursiveCallback\TypedCallback;

use Drupal\Core\Form\FormStateInterface;
use Drupal\webform4json\InlineEntityForm\Shim\RecursiveCallback\Interface\ElementValidateInterface;

final class ElementValidateCallback implements ElementValidateInterface {

  public function __construct(
    private readonly mixed $rawCallback,
  ) {}

  public function elementValidate(array &$element, FormStateInterface &$formState, array &$completeForm): void {
    $formState->prepareCallback($this->rawCallback)($element, $formState, $completeForm);
  }

}
