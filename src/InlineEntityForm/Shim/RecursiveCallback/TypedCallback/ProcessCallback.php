<?php

declare(strict_types=1);
namespace Drupal\webform4json\InlineEntityForm\Shim\RecursiveCallback\TypedCallback;

use Drupal\Core\Form\FormStateInterface;
use Drupal\webform4json\InlineEntityForm\Shim\RecursiveCallback\Interface\ProcessInterface;

final class ProcessCallback implements ProcessInterface {

  public function __construct(
    private readonly mixed $rawCallback,
  ) {}

  public function process(array &$element, FormStateInterface &$formState, array &$completeForm): array {
    return $formState->prepareCallback($this->rawCallback)($element, $formState, $completeForm);
  }

}
