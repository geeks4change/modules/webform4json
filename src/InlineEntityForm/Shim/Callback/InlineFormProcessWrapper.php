<?php

declare(strict_types=1);
namespace Drupal\webform4json\InlineEntityForm\Shim\Callback;

use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Form\FormStateInterface;
use Drupal\webform4json\InlineEntityForm\Shim\InlineFormStateShim;
use Drupal\webform4json\InlineEntityForm\Shim\InlineWebformSubmissionShim;
use Drupal\webform4json\InlineEntityForm\Shim\RecursiveCallback\Interface\ProcessInterface;
use Drupal\webform4json\InlineEntityForm\Shim\RecursiveCallback\Interface\ProcessWrapperInterface;

final class InlineFormProcessWrapper implements ProcessWrapperInterface {

  public function __construct(
    private readonly InlineWebformSubmissionShim $submissionShim,
  ) {}

  public function wrapProcess(ProcessInterface $process): ProcessInterface {
    return new class($process, $this->submissionShim) implements ProcessInterface {
      public function __construct(
        private readonly ProcessInterface $process,
        private readonly InlineWebformSubmissionShim $submissionShim,
      ) {}

      public function process(array &$element, FormStateInterface &$formState, array &$completeForm): array {
        // Get current form from #parents, as #after_build can invalidate references.
        $inlineForm =& NestedArray::getValue($formState->getCompleteForm(), $this->submissionShim->getArrayParents());
        $subFormState = new InlineFormStateShim($formState, $inlineForm, $this->submissionShim->getFormObject());
        $completeSubForm =& $subFormState->getCompleteForm();
        return $this->process->process($element, $subFormState, $completeSubForm);
      }
    };
  }

}
