<?php

declare(strict_types=1);
namespace Drupal\webform4json\InlineEntityForm\Shim\Callback;

final class StatesPrefixer {

  public static function prefixStateNames(array &$states, array $parents): void {
    if (!$parents) {
      return;
    }
    $parentsPrefix = self::createPrefix($parents);
    $result = [];
    foreach ($states as $key => &$state) {
      if (
        is_string($key)
        && preg_match('~^(.*:input\[name=")([^\[]*)(\[.*])?("])$~u', $key, $matches)
      ) {
        [
          ,
          $leadingSelectorAndOpeningSquareBrackets,
          $firstNameNoBrackets,
          $moreNamesInSquareBrackets,
          $closingSquareBracket,
        ] = $matches;
        $key = "$leadingSelectorAndOpeningSquareBrackets"
          . "{$parentsPrefix}[$firstNameNoBrackets]"
          . "{$moreNamesInSquareBrackets}{$closingSquareBracket}";
      }
      if (is_array($state)) {
        self::prefixStateNames($state, $parents);
      }
      $result[$key] = $state;
    }
    $states = $result;
  }

  /**
   * @param array $parents
   *   E.g. ['foo', 'bar', 'baz']
   *
   * @return string
   *   E.g. 'foo[bar][baz]'
   */
  private static function createPrefix(array $parents): string {
    $parts = [];
    if ($parents) {
      $parts[] = array_shift($parents);
      $trailing = array_map(fn(string $part) => "[$part]", $parents);
      $parts = array_merge($parts, $trailing);
    }
    return implode('', $parts);
  }

}
