<?php

declare(strict_types=1);
namespace Drupal\webform4json\InlineEntityForm\Shim\Callback;

use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Form\FormStateInterface;
use Drupal\webform4json\InlineEntityForm\Shim\InlineFormStateShim;
use Drupal\webform4json\InlineEntityForm\Shim\InlineWebformSubmissionShim;
use Drupal\webform4json\InlineEntityForm\Shim\RecursiveCallback\Interface\AfterBuildInterface;
use Drupal\webform4json\InlineEntityForm\Shim\RecursiveCallback\Interface\AfterBuildWrapperInterface;

final class InlineFormAfterBuildWrapper implements AfterBuildWrapperInterface {

  public function __construct(
    private readonly InlineWebformSubmissionShim $submissionShim,
  ) {}

  public function wrapAfterBuild(AfterBuildInterface $afterBuild): AfterBuildInterface {
    return new class($afterBuild, $this->submissionShim) implements AfterBuildInterface {
      public function __construct(
        private readonly AfterBuildInterface $afterBuild,
        private readonly InlineWebformSubmissionShim $submissionShim,
      ) {}

      public function afterBuild(array $element, FormStateInterface &$formState): array {
        // Get current form from #parents, as #after_build can invalidate references.
        $inlineForm =& NestedArray::getValue($formState->getCompleteForm(), $this->submissionShim->getArrayParents());
        $subFormState = new InlineFormStateShim($formState, $inlineForm, $this->submissionShim->getFormObject());
        return $this->afterBuild->afterBuild($element, $subFormState);
      }
    };
  }

}
