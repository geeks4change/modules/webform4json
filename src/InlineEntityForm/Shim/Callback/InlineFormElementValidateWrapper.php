<?php

declare(strict_types=1);
namespace Drupal\webform4json\InlineEntityForm\Shim\Callback;

use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Form\FormStateInterface;
use Drupal\webform4json\InlineEntityForm\Shim\InlineFormStateShim;
use Drupal\webform4json\InlineEntityForm\Shim\InlineWebformSubmissionShim;
use Drupal\webform4json\InlineEntityForm\Shim\RecursiveCallback\Interface\ElementValidateInterface;
use Drupal\webform4json\InlineEntityForm\Shim\RecursiveCallback\Interface\ElementValidateWrapperInterface;

final class InlineFormElementValidateWrapper implements ElementValidateWrapperInterface {

  public function __construct(
    private readonly InlineWebformSubmissionShim $submissionShim,
  ) {}

  public function wrapElementValidate(ElementValidateInterface $elementValidate): ElementValidateInterface {
    return new class($elementValidate, $this->submissionShim) implements ElementValidateInterface {
      public function __construct(
        private readonly ElementValidateInterface $elementValidate,
        private readonly InlineWebformSubmissionShim $submissionShim,
      ) {}

      public function elementValidate(array &$element, FormStateInterface &$formState, array &$completeForm): void {
        // Get current form from #parents, as #after_build can invalidate references.
        $inlineForm =& NestedArray::getValue($formState->getCompleteForm(), $this->submissionShim->getArrayParents());
        $subFormState = new InlineFormStateShim($formState, $inlineForm, $this->submissionShim->getFormObject());
        $completeSubForm =& $subFormState->getCompleteForm();
        $this->elementValidate->elementValidate($element, $subFormState, $completeSubForm);
      }
    };
  }

}
