<?php

declare(strict_types=1);
namespace Drupal\webform4json\InlineEntityForm\Shim\Callback;

use Drupal\webform4json\InlineEntityForm\Shim\RecursiveCallback\Interface\PreRenderInterface;

final class InlineFormStatesPrefixer implements PreRenderInterface {

  public function __construct(
    private readonly array $parents,
  ) {}

  /**
   * Prefix the #states in #pre_render.
   *
   * Doing it in #pre_renders, because we need
   * - rendered #states prefixed, so the JS finds the elements.
   * - the form build #states not prefixed, so webform finds the elements.
   *
   * @see \Drupal\webform\WebformSubmissionConditionsValidator::validateConditions
   * @see \Drupal\Core\Form\FormHelper::processStates
   * @see \Drupal\webform\Utility\WebformFormHelper::processStates
   * @see \Drupal\webform\Utility\WebformElementHelper::fixStatesWrapper
   */
  public function preRender(array $element): array {
    if (!empty($element['#states'])) {
      StatesPrefixer::prefixStateNames($element['#states'], $this->parents);
    }
    // Some #states even come pre-rendered in #prefix, e.g. webform_custom_composite.
    if (!empty($element['#prefix'])) {
      /** @noinspection PhpNamedArgumentsWithChangedOrderInspection */
      /** @noinspection PhpUndefinedVariableInspection */
      $fixed = preg_replace_callback(
        subject: $element['#prefix'],
        pattern: '~^(.* data-drupal-states=")(.*?)(".*)$~u',
        count: $matchCount,
        callback: function (array $matches) {
          $statesJson = html_entity_decode($matches[2]);
          $states = json_decode($statesJson, TRUE);
          if ($states) {
            StatesPrefixer::prefixStateNames($states, $this->parents);
            $encodedStates = json_encode($states, JSON_HEX_QUOT);
            $fixed = $matches[1] . htmlspecialchars($encodedStates) . $matches[3];
          }
          return $fixed ?? $matches[0];
        },
      );
      if ($matchCount) {
        $element['#prefix'] = $fixed;
      }
    }
    return $element;
  }

}
