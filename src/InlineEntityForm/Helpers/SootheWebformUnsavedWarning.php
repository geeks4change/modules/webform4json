<?php

declare(strict_types=1);
namespace Drupal\webform4json\InlineEntityForm\Helpers;

use Drupal\Component\Utility\Html;
use Drupal\Core\Ajax\InvokeCommand;
use Drupal\Core\Form\FormStateInterface;
use Drupal\webform4json\InlineEntityForm\Shim\InlineFormStateShim;

/**
 * Soothe webform unsaved warning.
 *
 * Webform adds JS that triggers an "unsaved content" alert on unload.
 * To have submit-buttons marked as having saved, the complete form needs
 * a magic class.
 */
final class SootheWebformUnsavedWarning {

  const MagicDrupalSettingsFormId = 'webform4json_soothe_webform_unsaved_form_id';

  protected const WebformUnsavedClass = 'js-webform-unsaved';

  /**
   * Called in form build phase, fixes a form containing a webform.
   */
  public static function fixInBuild(array &$element): void {
    // Add fix to sub-array to not break IEF element #process.
    $element['soothe_webform_unsaved_warning']['#process']
    [] = [self::class, 'processCallbackToAddWebformUnsavedClass'];
  }

  public static function processCallbackToAddWebformUnsavedClass(array $elements, FormStateInterface $formState, array &$completeForm = []): array {
    // Save the root formId in drupalSettings.
    $rootFormState = InlineFormStateShim::getRootFormState($formState);
    // $completeForm parameter may be faked.
    $rootForm =& $rootFormState->getCompleteForm();
    if (
      ($formId = $rootForm['#form_id'] ?? NULL)
      && is_string($formId)
    ) {
      // Add the class attribute.
      /** @see webform.form.unsaved.js */
      $rootForm['actions']['#attributes']['class'][] = 'js-webform-unsaved';

      // Signal to ::hookAjaxRenderAlter via drupalSettings.
      // It seems this is the only reliable id.
      // Really, data-drupal-selector should, but is not.
      $htmlFormId = Html::getId($formId);
      /** @see \Drupal\Core\Render\BubbleableMetadata::mergeAttachments */
      $elements['#attached']['drupalSettings'][self::MagicDrupalSettingsFormId][$htmlFormId] = TRUE;
    }
    return $elements;
  }

  /**
   * Implements hook_ajax_render_alter().
   *
   * This is needed, if the form page does NOT yet have an inline webform,
   * and it is added later via ajax (maybe by a paragraph).
   *
   * In that case, ensure that the actions class makes it to the browser.
   *
   * @param array[] $renderedCommands
   *    **Contrary** to phpdoc, commands are rendered.
   *
   * @see \Drupal\Core\Ajax\AjaxResponseAttachmentsProcessor::buildAttachmentsCommands
   * @see \Drupal\Core\EventSubscriber\AjaxResponseSubscriber::onResponse
   * @see \Drupal\Core\Ajax\AjaxResponse::getCommands
   */
  public static function hookAjaxRenderAlter(array &$renderedCommands): void {
    if (
      ($setting = self::findSetting($renderedCommands, self::MagicDrupalSettingsFormId))
      && is_array($setting)
      && ($htmlFormIds = array_keys($setting))
    ) {
      foreach ($htmlFormIds as $htmlFormId) {
        $command = new InvokeCommand("#$htmlFormId #edit-actions", 'addClass', [self::WebformUnsavedClass]);
        // Prepend command so later behaviors find the new class.
        array_unshift($renderedCommands, $command->render());
      }
    }
  }

  protected static function findSetting(array $renderedCommands, string $setting): mixed {
    foreach ($renderedCommands as $renderedCommand) {
      if ($renderedCommand['command'] === 'settings') {
        $settings = $renderedCommand['settings'];
        if ($setting = $settings[self::MagicDrupalSettingsFormId] ?? NULL) {
          return $setting;
        }
      }
    }
    return NULL;
  }

}
