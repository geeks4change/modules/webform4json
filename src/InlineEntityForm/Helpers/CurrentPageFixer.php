<?php

declare(strict_types=1);
namespace Drupal\webform4json\InlineEntityForm\Helpers;

use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Form\FormStateInterface;
use Drupal\webform\WebformSubmissionInterface;

/**
 * Fix WebformSubmission current_page.
 *
 * There are multiple places where webform gets current_page from formState:
 * - when building,
 *   - => Not needed. Inline form is created in #process.
 * - in #process and #after_build
 *   - => use [inProcess]prepareCurrentPageFix()
 * - in #validate
 *   - => Use WebformValidationFixer
 * - in #entity_builders
 *   - => use setTmpCurrentPage() *
 *
 * @see \Drupal\webform\WebformSubmissionForm::getCurrentPage
 * @see \Drupal\webform\WebformSubmissionForm::form
 * @see \Drupal\webform\WebformSubmissionConditionsValidator::processForm
 * @see \Drupal\webform\WebformSubmissionForm::copyFormValuesToEntity
 */
final class CurrentPageFixer {

  public static function fixAllPagesInProcess(array &$elementsForm, WebformSubmissionInterface $webformSubmission): void {
    $pages = $webformSubmission->getWebform()->getPages('edit', $webformSubmission);
    if ($pages) {
      $elements = $webformSubmission->getWebform()
        ->getElementsInitializedAndFlattened();
      foreach ($pages as $pageKey => $page) {
        $pageParents = $elements[$pageKey]['#webform_parents'];
        $pageElement =& NestedArray::getValue($elementsForm, $pageParents);
        CallbacksHelper::prepend($pageElement['#process'], [self::class, 'callbackInProcessSetCurrentPage']);
      }
    }
    // If no pages, nothing to do.
  }

  /**
   * @internal #process callback.
   */
  public static function callbackInProcessSetCurrentPage(array $pageElement, FormStateInterface $form_state, &$complete_form): array {
    $currentPage = $pageElement['#webform_key'];
    $form_state->set('current_page', $currentPage);
    return $pageElement;
  }

}
