<?php

declare(strict_types=1);
namespace Drupal\webform4json\InlineEntityForm\Helpers;

use Drupal\Core\Form\FormInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element;
use Drupal\webform\WebformSubmissionConditionsValidator;

final class WebformValidationFixer {

  protected const PROP_ORIGINAL = '#webform4json__element_validate';

  public function __construct(
    protected array &$entityForm,
    protected FormInterface $formObject,
    protected string $currentPage,
  ) {}

  /**
   * Fix webform elements validation.
   *
   * Webform adds its own validation callback to each element, to replace the
   * hardcoded #required validation of FormValidator.
   * The ::elementValidate callback extracts the webform from current form object
   * though, which breaks on sub-forms.
   * Wrap that callback and swap out the form object.
   *
   * @see \Drupal\webform\WebformSubmissionConditionsValidator::buildForm
   * @see \Drupal\webform\WebformSubmissionConditionsValidator::elementAfterBuild
   * @see \Drupal\webform\WebformSubmissionConditionsValidator::elementValidate
   */
  public function fixElements(): void {
    $this->entityForm['elements']['#after_build'][] = [$this, 'afterBuildFixElements'];
  }

  /**
   * After-build callback to fix elements.
   *
   * WebformSubmissionConditionsValidator::buildForm does this in #after_build.
   * While #process gets processed downwards before recursion, #after_build gets
   * processed upwards, after recursion, so by the time when we #after_build the
   * outermost wrapper, we see and can replace all callbacks.
   */
  public function afterBuildFixElements(array $elements, FormStateInterface $form_state) {
    if (is_array($elements['#element_validate'] ?? NULL)) {
      $elements[self::PROP_ORIGINAL] = $elements['#element_validate'];
      $elements['#element_validate'] = [[$this, 'wrapElementValidate']];
    }
    foreach (Element::children($elements) as $key) {
      if (is_array($elements[$key])) {
        $elements[$key] = self::afterBuildFixElements($elements[$key], $form_state);
      }
    }
    return $elements;
  }

  public function wrapElementValidate(array &$elements, FormStateInterface &$form_state): void {
    $tmpFormState = new FormStateTmp($form_state);
    $tmpFormState->setFormObject($this->formObject);
    $tmpFormState->set('current_page', $this->currentPage);
    // Copied from FormValidator.
    foreach ($elements[self::PROP_ORIGINAL] as $callback) {
      // Copied from FormValidator.
      $completeForm = &$form_state->getCompleteForm();
      call_user_func_array($form_state->prepareCallback($callback), [&$elements, &$form_state, &$completeForm]);
    }
  }

}
