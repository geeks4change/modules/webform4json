<?php

declare(strict_types=1);
namespace Drupal\webform4json\InlineEntityForm\Utility;

use Drupal\Core\Render\ElementInfoManagerInterface;

final class ElementDefaults {

  public static function load(array &$element): void {
    if (isset($element['#type']) && empty($element['#defaults_loaded'])) {
      $element += self::getElementInfo()->getInfo($element['#type']);
    }
  }

  private static function getElementInfo(): ElementInfoManagerInterface {
    return \Drupal::service('plugin.manager.element_info');
  }

}
