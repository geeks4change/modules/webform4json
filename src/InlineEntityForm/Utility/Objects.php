<?php

declare(strict_types=1);
namespace Drupal\webform4json\InlineEntityForm\Utility;

final class Objects {

  private function __construct(
    private readonly array $items,
  ) {}

  public static function create(object ...$items): self {
    return new self($items);
  }

  /**
   * @template T
   *
   * @param class-string<T> $classOrInterface
   *
   * @return list<T>
   */
  public function instanceOf(string $classOrInterface): array {
    return array_filter(
      array: $this->items,
      callback: fn($item) => $item instanceof $classOrInterface,
    );
  }

}
