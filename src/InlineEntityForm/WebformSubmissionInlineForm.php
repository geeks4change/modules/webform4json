<?php

declare(strict_types=1);

namespace Drupal\webform4json\InlineEntityForm;

use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\inline_entity_form\Form\EntityInlineForm;
use Drupal\webform\WebformSubmissionConditionsValidatorInterface;
use Drupal\webform\WebformSubmissionForm;
use Drupal\webform\WebformSubmissionInterface;
use Drupal\webform4json\InlineEntityForm\Helpers\AttributesFixer;
use Drupal\webform4json\InlineEntityForm\Helpers\CallbacksFixer;
use Drupal\webform4json\InlineEntityForm\Helpers\CurrentPageFixer;
use Drupal\webform4json\InlineEntityForm\Helpers\FormStateTmp;
use Drupal\webform4json\InlineEntityForm\Helpers\SootheWebformUnsavedWarning;
use Drupal\webform4json\InlineEntityForm\Helpers\WebformParentsFixer;
use Drupal\webform4json\InlineEntityForm\Helpers\WebformStatesFixer;
use Drupal\webform4json\InlineEntityForm\Helpers\WebformValidationFixer;
use Drupal\webform4json\InlineEntityForm\Shim\Callback\InlineFormAfterBuildWrapper;
use Drupal\webform4json\InlineEntityForm\Shim\Callback\InlineFormElementValidateWrapper;
use Drupal\webform4json\InlineEntityForm\Shim\Callback\InlineFormProcessWrapper;
use Drupal\webform4json\InlineEntityForm\Shim\Callback\InlineFormStatesPrefixer;
use Drupal\webform4json\InlineEntityForm\Shim\InlineWebformSubmissionShim;
use Drupal\webform4json\InlineEntityForm\Shim\RecursiveCallback\RecursiveCallbackWrapper;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Handle a WebformSubmission inline form.
 *
 * Parent uses EntityFormDisplay, which won't work for webform elements.
 * @see \Drupal\inline_entity_form\Form\EntityInlineForm::entityForm
 *
 * Instead leverage
 */
final class WebformSubmissionInlineForm extends EntityInlineForm {

  public const PROP_CURRENT_PAGE = '#current_page';

  protected WebformSubmissionConditionsValidatorInterface $conditionsValidator;

  public static function createInstance(ContainerInterface $container, EntityTypeInterface $entity_type) {
    $instance = parent::createInstance($container, $entity_type);
    $instance->conditionsValidator = $container->get('webform_submission.conditions_validator');
    return $instance;
  }

  public static function setCurrentPage(array &$element, string $currentPage) {
    $element[self::PROP_CURRENT_PAGE] = $currentPage;
  }

  /**
   * Build the webformSubmission inline form.
   *
   * Called in form #process phase.
   * - Called by IEF
   *   @see \Drupal\inline_entity_form\Element\InlineEntityForm::processEntityForm
   * - Calling webformSubmissionForm::form, not ::buildform
   *   @see \Drupal\webform\WebformSubmissionForm::form
   *
   * Code not inline-ready that need to be fooled:
   * - WebformSubmissionForm inherits ContentEntityForm::form, which does
   *   `$this->getFormDisplay($form_state)->buildForm($this->entity, $form, $form_state);
   *   which does
   *   `return $form_state->get('form_display');`
   *   and returns the wrong display.
   *   - @see \Drupal\Core\Entity\ContentEntityForm::form
   *   =>
   *
   * Measures:
   * - A fake webformSubmission formObject
   * - A RAII tmp form state handling temporary adjustments: form_display, current_page
   *   (which by design works while the inline form is built, but not in process)
   *   @see \Drupal\webform4json\InlineEntityForm\Helpers\FormStateTmp
   * - An inline_form_state shim.
   *   @see \Drupal\webform4json\InlineEntityForm\Shim\InlineFormStateShim
   * - A CurrentPageFixer that sets FormState::current_page to the respective
   *   page in #process, for every page.
   *   @see \Drupal\webform4json\InlineEntityForm\Helpers\CurrentPageFixer::fixAllPagesInProcess
   * - A #entity_builder
   *   @see self::buildWebformSubmission
   * - A WebformParentsFixer that flattens parents wrt current parents.
   *   @see \Drupal\webform4json\InlineEntityForm\Helpers\WebformParentsFixer::process
   * - A CallbacksFixer prefixing current formObject to ::foo callbacks.
   *   @see \Drupal\webform4json\InlineEntityForm\Helpers\CallbacksFixer::makeAbsolute
   * - A WebformStatesFixer prefixing #states with current parents.
   *   @see \Drupal\webform4json\InlineEntityForm\Helpers\WebformStatesFixer::prefixStateNames
   * - Run webform apply-states logic to have correct #required values.
   *   @see \Drupal\webform\WebformSubmissionConditionsValidatorInterface::buildForm
   * - Wrap all #element_validate and fake formObject and FormState::current_page.
   *   @see \Drupal\webform4json\InlineEntityForm\Helpers\WebformValidationFixer::fixElements
   *
   * Minor measures:
   * - Add class 'data-is-webform-element' to soothe JS.
   * - Add dummy action to soothe unsaved-logic.
   * - Remove 'js-webform-autofocus' class.
   * - Soothe Webform-Unsaved-Warning
   *   @see \Drupal\webform4json\InlineEntityForm\Helpers\SootheWebformUnsavedWarning::fixInProcess
   */
  public function entityForm(array $entity_form, FormStateInterface $form_state) {
    $entity_form = parent::entityForm($entity_form, $form_state);
    $currentPage = $entity_form[self::PROP_CURRENT_PAGE];
    /** @var \Drupal\webform\Entity\WebformSubmission $webformSubmission */
    $webformSubmission = $entity_form['#entity'];

    $shim = InlineWebformSubmissionShim::create($entity_form, $webformSubmission);

    // Changes form state, will be reverted when it goes out of scope.
    $tmpFormStateOverride = new FormStateTmp($form_state);
    // Temporarily set our form display to fool ContentEntityForm.
    // @todo Consider moving to shim.
    /** @see \Drupal\Core\Entity\ContentEntityForm::getFormDisplay */
    $tmpFormStateOverride->set('form_display', $this->getFormDisplay($webformSubmission, $entity_form['#form_mode']));

    // Tell builder the current webform page.
    $tmpFormStateOverride->set('current_page', $currentPage);

    // BUILD the form.
    $entity_form = $shim->getFormObject()->form($entity_form, $form_state);

    // Prevent multi page unsaved warning.
    unset($entity_form['#attributes']['data-webform-unsaved']);

    // Add webformSubmission entity builder.
    $entity_form['#entity_builders'][] = [get_class($this), 'buildWebformSubmission'];


    // Make callbacks absolute: ::foo must be WebformSubmissionForm::foo
    CallbacksFixer::makeAbsolute($entity_form, $shim->getFormObject());

    // Set #parents for all children.
    // Webform needs flattened element values.
    // We flatten them wrt webform elements #parents namespace.
    $entity_form['#tree'] = TRUE;
    $elementsParents = array_merge($entity_form['#parents'], ['elements']);
    WebformParentsFixer::fixWebformElementsParents($entity_form['elements'], $elementsParents);


    // == Adjust webform #states ==
    /** @see \Drupal\webform\WebformSubmissionConditionsValidator::buildForm */
    $tmpFormStateOverride->setFormObject($shim->getFormObject());
    // Apply #states visible / required.
    // Must be done before adjusting #states selectors.
    // This adds #after_build, which adds #element_validate, which builds entity.
    /** @see \Drupal\webform\WebformSubmissionConditionsValidator::elementAfterBuild */
    $this->conditionsValidator->buildForm($entity_form, $form_state);

    // == Make some minor fixes ==
    // Disable the progress bar.
    $entity_form['progress']['#access'] = FALSE;

    //  $.fn.isWebform = function () {
    //    return $(this).closest('form.webform-submission-form, form[id^="webform"], form[data-is-webform]').length ? true : false;
    //  };
    //   $.fn.isWebformElement = function () {
    //    return ($(this).isWebform() || $(this).closest('[data-is-webform-element]').length) ? true : false;
    //  };
    $entity_form['#attributes']['data-is-webform-element'] = TRUE;

    // Add fake actions to suppress notices in WebformSubmissionForm::afterBuild
    $entity_form['actions'] = [];
    $entity_form['#action'] = 'form_action_DUMMY';

    // Drop autofocus.
    AttributesFixer::removeClasses($entity_form, 'js-webform-autofocus');

    // Prevent false "unsaved form" alarms.
    SootheWebformUnsavedWarning::fixInBuild($entity_form);

    // == Finally, have form callbacks fixed. ==
    RecursiveCallbackWrapper::create(
      new InlineFormProcessWrapper($shim),
      new InlineFormAfterBuildWrapper($shim),
      new InlineFormElementValidateWrapper($shim),
      new InlineFormStatesPrefixer($elementsParents),
    )->prepareDescendants($entity_form);

    // Hide the pages element, to not display bogus "Edit" buttons.
    $entity_form['pages']['#access'] = FALSE;

    return $entity_form;
  }

  /**
   * Webform entity builder callback.
   */
  public static function buildWebformSubmission(string $entityTypeId, WebformSubmissionInterface $webformSubmission, array $form, FormStateInterface $formState) {
    // $form has #parents, its children do not yet.
    $parents = $form['#parents'];
    $parents[] = 'elements';
    // Set all data items, the consumer widget may filter this.
    $data = $formState->getValue($parents, []);
    $webformSubmission->setData($data);
  }

  /**
   * Copy of parent, with webform specific validation.
   *
   * @see \Drupal\webform\WebformSubmissionForm::validateForm
   */
  public function entityFormValidate(array &$entity_form, FormStateInterface $form_state) {
    // Perform entity validation only if the inline form was submitted,
    // skipping other requests such as file uploads.
    $triggering_element = $form_state->getTriggeringElement();
    if (!empty($triggering_element['#ief_submit_trigger'])) {
      /** @var \Drupal\webform\WebformSubmissionInterface $webformSubmission */
      $webformSubmission = $entity_form['#entity'];
      $this->buildEntity($entity_form, $webformSubmission, $form_state);
      $form_display = $this->getFormDisplay($webformSubmission, $entity_form['#form_mode']);
      $form_display->validateFormValues($webformSubmission, $entity_form, $form_state);
      $webformSubmission->setValidationRequired(FALSE);

      // ADDED
      $shim = InlineWebformSubmissionShim::create($entity_form, $webformSubmission);
      $tmpFormStateOverride = new FormStateTmp($form_state);
      $tmpFormStateOverride->setFormObject($shim->getFormObject());
      $this->conditionsValidator->validateForm($entity_form, $form_state);
      $webformSubmission->getWebform()->invokeHandlers('validateForm', $form, $form_state, $webformSubmission);
    }
  }

}
