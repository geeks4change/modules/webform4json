<?php

declare(strict_types=1);
namespace Drupal\webform4json\Helper;

use Drupal\webform\WebformSubmissionInterface;

final class WebformSubmissionNormalizationWrapper {

  public function __construct(
    protected WebformSubmissionInterface $webformSubmission,
  ) {}

  /**
   * Get webform data as normalized (single elements are array too).
   *
   * Normalized data does not break when element is switched between single and
   * multiple.
   */
  public function getData(): array {
    $data = $this->webformSubmission->getData();
    // Note that array_map with key does not preserve key. This does.
    return iterator_to_array((function() use ($data) {
      foreach ($data as $key => $value) {
        $normalizedValue = $this->elementIsMultiple($key)
          ? $value : [$value];
        yield $key => $normalizedValue;
      }
    })());
  }

  /**
   * Set webform data from normalized (single elements are array too).
   *
   * Normalized data does not break when element is switched between single and
   * multiple.
   */
  public function setDate(array $data): void {
    // Note that array_map with key does not preserve key. This does.
    $denormalizedData = iterator_to_array((function() use ($data) {
      foreach ($data as $key => $value) {
        $denormalizedValue = $this->elementIsMultiple($key)
          ? $value : reset($value);
        yield $key => $denormalizedValue;
      }
    })());
    $this->webformSubmission->setData($denormalizedData);
  }

  protected function elementIsMultiple(string $elementKey): bool {
    $elementDefinition = self::getElementDefinition($elementKey);
    return boolval($elementDefinition['#multiple'] ?? FALSE);
  }

  protected function getElementDefinition(string $elementKey): array {
    $webform = $this->webformSubmission->getWebform();
    return $webform->getElementDecoded($elementKey)
      ?? throw new \OutOfRangeException(
        sprintf(
          'Unknown element: "%s". Available: %s',
          $elementKey,
          implode('|', array_keys($webform->getElementsDecodedAndFlattened()))
        )
      );
  }

}
