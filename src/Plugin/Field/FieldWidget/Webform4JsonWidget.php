<?php

declare(strict_types=1);
namespace Drupal\webform4json\Plugin\Field\FieldWidget;

use Drupal\Core\Field\Annotation\FieldWidget;
use Drupal\Core\Field\FieldItemInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\h4d_shared\DumperTool;
use Drupal\inline_entity_form\Element\InlineEntityForm;
use Drupal\webform\Entity\Webform;
use Drupal\webform\Entity\WebformSubmission;
use Drupal\webform\WebformSubmissionConditionsValidatorInterface;
use Drupal\webform\WebformSubmissionInterface;
use Drupal\webform4json\Helper\WebformSubmissionNormalizationWrapper;
use Drupal\webform4json\InlineEntityForm\Helpers\SootheWebformUnsavedWarning;
use Drupal\webform4json\InlineEntityForm\WebformSubmissionInlineForm;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the 'W4C JSON' widget.
 *
 * @FieldWidget(
 *   id = "webform4json",
 *   label = @Translation("W4J JSON"),
 *   field_types = {
 *     "json",
 *     "json_native",
 *     "json_native_binary",
 *   }
 * )
 */
final class Webform4JsonWidget extends WidgetBase {

  protected WebformSubmissionConditionsValidatorInterface $conditionsValidator;

  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $instance->conditionsValidator = $container->get('webform_submission.conditions_validator');
    return $instance;
  }

  public static function defaultSettings() {
    return [
      'webform' => '',
      'current_page' => '',
      ] + parent::defaultSettings();
  }

  public function settingsForm(array $form, FormStateInterface $form_state) {
    $form = parent::settingsForm($form, $form_state);
    $form['webform'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Webform'),
      '#default_value' => $this->getSetting('webform'),
      '#required' => TRUE,
    ];
    $form['current_page'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Current page key'),
      '#default_value' => $this->getSetting('current_page')
    ];
    return $form;
  }

  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $webformId = $this->getSetting('webform');
    $webform = Webform::load($webformId);

    if (!$webform) {
      return [];
    }

    $webformSubmission = WebformSubmission::create([
      'webform_id' => $webformId,
    ]);

    /** @var \Drupal\Core\Field\FieldItemInterface $item */
    $item = $items->get($delta);
    (new WebformSubmissionNormalizationWrapper($webformSubmission))
      ->setDate($this->getJsonData($item));

    $build = [
      '#type' => 'inline_entity_form',
      '#entity_type' => 'webform_submission',
      '#bundle' => $webformId,
      '#default_value' => $webformSubmission,
      '#op' => 'edit',
      '#save_entity' => FALSE,
    ];

    WebformSubmissionInlineForm::setCurrentPage($build, $this->getSetting('current_page'));

    SootheWebformUnsavedWarning::fixInBuild($build);

    return $build;
  }

  public function massageFormValues(array $values, array $form, FormStateInterface $form_state) {
    $inlineFormHandler = InlineEntityForm::getInlineFormHandler('webform_submission');
    $massaged = [];
    foreach ($values as $index => $value) {
      $inlineForm =& $form[$this->fieldDefinition->getName()]['widget'][$index];
      $inlineFormHandler->entityFormSubmit($inlineForm, $form_state);
      /** @var \Drupal\webform\WebformSubmissionInterface $webformSubmission */
      $webformSubmission = $inlineForm['#entity'];
      $data1 = (new WebformSubmissionNormalizationWrapper($webformSubmission))
        ->getData();
      $data2 = array_filter(
        $data1,
        fn(string $key) => $this->isElementOnPage($key, $webformSubmission),
        ARRAY_FILTER_USE_KEY
      );
      $data3 = array_filter(
        $data2,
        fn(string $key) => $this->isElementVisible($key, $webformSubmission),
        ARRAY_FILTER_USE_KEY
      );
      $json = json_encode($data3);
      $massaged[$index] = [
        '_original_delta' => $value['_original_delta'] ?? NULL,
        'value' => $json,
      ];
    }
    return $massaged;
  }

  private function getJsonData(FieldItemInterface $item): array {
    $json = $item->get('value')->getValue();
    $data = $json ? json_decode($json, TRUE) : [];
    return is_array($data) ? $data : [];
  }

  private function isElementVisible(string $elementKey, WebformSubmissionInterface $webformSubmission): bool {
    $element = $webformSubmission->getWebform()->getElementDecoded($elementKey);
    /** @noinspection PhpTernaryExpressionCanBeReplacedWithConditionInspection */
    $isVisible = $element
      ? $this->conditionsValidator->isElementVisible($element, $webformSubmission)
      : FALSE;
    return $isVisible;
  }

  private function isElementOnPage(string $elementKey, WebformSubmissionInterface $webformSubmission): bool {
    $currentPage = $this->getSetting('current_page');
    if ($currentPage) {
      $element = $webformSubmission->getWebform()->getElement($elementKey);
      $parents = $element['#webform_parents'] ?? [''];
      $isOnPage = $parents[0] === $currentPage;
    }
    else {
      $isOnPage = TRUE;
    }
    return $isOnPage;
  }

}
